# 无人地磅系统
Modbus的Android实现，添加对Android串口（RTU）的支持，支持RxJava操作


不了解Modbus的，可以阅读一下 [MODBUS通讯协议](https://github.com/licheedev/Modbus4Android/blob/master/imgs/modbus_proto_cn.pdf)

参考
> https://github.com/infiniteautomation/modbus4j
>
> https://github.com/zgkxzx/Modbus4Android

## 使用

两个车牌识别摄像头    两个网线

两个光幕       两个开关量

两个栏杆机     两个开关量

两个喇叭           自带

两个信号灯          两个开关量

一个地磅串口           1路串口

两个读卡器串口输入      两路串口 输入

两个打印机               两路串口 输出

两个屏幕               两路串口  输出


摄像头发送车牌
开启杆1
--------------------------------> 等待上磅
DTU1 光幕信号
------------------------------->等待称重
地磅数据

--------------------------------->等待保存
       语音PC 报重量
 开杆机2
--------------------------------->等待下磅
DTU2光幕

--------------------------------> 等待上磅



