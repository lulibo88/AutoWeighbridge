package com.licheedev.demo.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.licheedev.demo.ChooseModeActivity;
import com.licheedev.demo.R;
import com.licheedev.demo.main.FunctionActivity;
import com.licheedev.demo.main.MainFunctionActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN , WindowManager.LayoutParams. FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                finish();
                startActivity(new Intent(SplashActivity.this, MainFunctionActivity.class));
            }
        }, 3000);    //延时1s执行
    }
}