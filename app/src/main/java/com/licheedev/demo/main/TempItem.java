package com.licheedev.demo.main;

import com.bin.david.form.annotation.SmartColumn;
import com.bin.david.form.annotation.SmartTable;

@SmartTable(name="缓存历史")
public class TempItem {
    @SmartColumn(id = 0,name ="流水号")
    public String id;
    @SmartColumn(id = 1,name ="车牌号")
    public String name;
    @SmartColumn(id = 2,name ="皮重")
    public String pizhong;
    @SmartColumn(id = 3,name ="毛重")
    public String maozhong;
    @SmartColumn(id = 4,name ="净重")
    public String jinzhong;
    @SmartColumn(id = 5,name ="过磅费")
    public String guobangfei;
    @SmartColumn(id = 6,name ="发货单位")
    public String fahuo;
    @SmartColumn(id = 7,name ="收货单位")
    public String shouhuo;
    @SmartColumn(id = 8,name ="运输单位")
    public String yunshu;
    @SmartColumn(id = 9,name ="货物")
    public String huowu;
}
