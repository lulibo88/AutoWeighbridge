package com.licheedev.demo.tablebean;

import com.bin.david.form.annotation.SmartColumn;
import com.bin.david.form.annotation.SmartTable;

@SmartTable
public class TempShowBean {
    @SmartColumn(id =1,name = "时间",autoMerge = true)
    public String time;
    @SmartColumn(id =1,name = "车牌号")
    public String carNum;
    @SmartColumn(id =2,name = "称重值")
    public String weight;
    @SmartColumn(id =3,name = "称重次数")
    public String chengzhoncishu;
    @SmartColumn(id =4,name = "人员")
    public String name;
    @SmartColumn(id =5,name = "运输单位")
    public String yunshu;
    @SmartColumn(id =6,name = "发货单位")
    public String fahuo;
    @SmartColumn(id =7,name = "收货单位")
    public String shouhuo;
}
