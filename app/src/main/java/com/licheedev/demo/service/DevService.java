package com.licheedev.demo.service;

import android.app.Service;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.jeremyliao.liveeventbus.LiveEventBus;
import com.licheedev.demo.App;
import com.licheedev.demo.histroy.CarInfo;
import com.licheedev.demo.tcp.TcpMsg;

public class DevService extends Service {

    public static final String TAG = "DevService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() executed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() executed");

        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() executed");
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
