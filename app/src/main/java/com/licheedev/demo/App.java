package com.licheedev.demo;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;

import com.licheedev.adaptscreen.AdaptScreenEx;
import com.licheedev.demo.base.PrefUtil;
import com.licheedev.demo.dao.DaoMaster;
import com.licheedev.demo.dao.DaoSession;
import com.serotonin.modbus4j.ModbusConfig;

public class App extends Application {

    static App sInstance;
    DaoMaster.DevOpenHelper helper;
    SQLiteDatabase db;
    DaoMaster daoMaster;
    private static DaoSession daoSession;
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        // 屏幕适配
        AdaptScreenEx.init(this);
        PrefUtil.init(this);
        MultiDex.install(this);
        configModbus();
        helper = new DaoMaster.DevOpenHelper(this, "histroy.db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }
    public static DaoSession getmDaoSession() {
        return daoSession;
    }
    public static App getInstance() {
        return sInstance;
    }

    /**
     * 配置Modbus,可选
     */
    private void configModbus() {
        // 启用rtu的crc校验（默认就启用）
        ModbusConfig.setEnableRtuCrc(true);
        // 打印数据log（默认全禁用）
        // System.out: MessagingControl.send: 01030000000305cb
        // System.out: MessagingConnection.read: 010306000100020000bd75
        ModbusConfig.setEnableDataLog(true, true);
    }
}
