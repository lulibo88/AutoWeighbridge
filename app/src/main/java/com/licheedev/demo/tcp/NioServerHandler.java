package com.licheedev.demo.tcp;

import com.jeremyliao.liveeventbus.LiveEventBus;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

import java.net.InetSocketAddress;

public class NioServerHandler extends SimpleChannelInboundHandler<Object> {
    /**
     *读取客户端发来的数据
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {

        String[] data = msg.toString().split("id=");
        if(data !=null && data.length>1) {
            String[] data1 = data[1].split(";");
            String id = data1[0];
            if (NioServer.map.get(id) != null && NioServer.map.get(id).equals(ctx)) {
                //接收到服务端发来的数据进行业务处理
                //     logger.info("接收数据成功！" + DateUtils.dateToString(new Date()));

            } else {
                //接收到服务端发来的数据进行业务处理
                //如果map中没有此ctx 将连接存入map中
                NioServer.map.put(id, ctx);

                //   logger.info("连接成功，加入map管理连接！"+"mn:" +mn+" " +""+ DateUtils.dateToString(new Date()));
            }
        }else{
            //logger.info(""+"不是监测数据"+ msg.toString()+" "+ DateUtils.dateToString(new Date()));
        }
        // ctx.writeAndFlush("Received your message : " + msg.toString());
    }
    /**
     * 功能：读取服务器发送过来的信息
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {


        // 第一种：接收字符串时的处理

        ByteBuf buf = (ByteBuf) msg;
        byte[] buffer = new byte[buf.readableBytes()];
        buf.readBytes(buffer, 0, buffer.length);
        String rev = new String(buffer);

        InetSocketAddress insocket = (InetSocketAddress) ctx.channel()
                .remoteAddress();
        System.out.println("收到客户端数据:" + rev + insocket.getAddress().getHostAddress());

        LiveEventBus.get("frim_client").post(new TcpMsg(insocket.getAddress().getHostAddress().replace("/",""),rev,2));

    }

    /*    *//**
     * 读取完毕客户端发送过来的数据之后的操作
     *//*
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        System.out.println("服务端接收数据完毕..");
        //  ctx.channel().write("");
        //  ctx.channel().flush();
    }*/

    /**
     * 客户端主动断开服务端的链接,关闭流
     * */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println(ctx.channel().localAddress().toString() + " 通道不活跃！");
        removeChannnelMap(ctx);

        ctx.close();
    }

    /**
     * 客户端主动连接服务端
     * */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//         logger.info("RemoteAddress"+ ctx.channel().remoteAddress() + " active !");
//         logger.info("msg"+ ctx.m + " active !");
        String[] arr= ctx.channel().remoteAddress().toString().split(":");
        String id = arr[0];
        if (NioServer.map.get(id.replace("/","")) != null && NioServer.map.get(id.replace("/","")).equals(ctx)) {
            //接收到服务端发来的数据进行业务处理
            //     logger.info("接收数据成功！" + DateUtils.dateToString(new Date()));
            System.out.println("new Data from id="+id);

        } else {
            //接收到服务端发来的数据进行业务处理
            //如果map中没有此ctx 将连接存入map中
            NioServer.map.put(id.replace("/",""), ctx);
            System.out.println("put id="+id);
            LiveEventBus.get("frim_client").post(new TcpMsg(id.replace("/",""),"",0));
            //   logger.info("连接成功，加入map管理连接！"+"mn:" +mn+" " +""+ DateUtils.dateToString(new Date()));
        }
//        ctx.writeAndFlush("连接成功！");
        super.channelActive(ctx);
    }

    /**
     * 发生异常处理
     * */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        // logger.error("连接异常,连接异常："+ DateUtils.dateToString(new Date())+cause.getMessage(), cause);
        ctx.fireExceptionCaught(cause);
        //   removeChannnelMap(ctx);
        //  ctx.close();
    }

    /**
     *删除map中ChannelHandlerContext
     *  */
    private void removeChannnelMap(ChannelHandlerContext ctx){
        for( String key :NioServer.map.keySet()){
            if( NioServer.map.get(key)!=null &&  NioServer.map.get(key).equals( ctx)){
                NioServer.map.remove(key);
                String[] arr= ctx.channel().remoteAddress().toString().split(":");
                String id = arr[0];
                LiveEventBus.get("frim_client").post(new TcpMsg(id.replace("/",""),"",1));
            }
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        super.userEventTriggered(ctx, evt);
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.READER_IDLE))
                ctx.close();
            //标志该链接已经close 了
        }
    }

}
