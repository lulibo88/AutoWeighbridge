package com.licheedev.demo.tcp;

public class TcpMsg {
    public String addr;
    public String msg;
    public int type;  //类型 连接0 断开1 接收到数据2

    public TcpMsg(String addr, String msg, int type) {
        this.addr = addr;
        this.msg = msg;
        this.type = type;
    }
}
