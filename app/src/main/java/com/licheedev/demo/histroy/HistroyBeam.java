package com.licheedev.demo.histroy;

import com.bin.david.form.annotation.SmartColumn;
import com.bin.david.form.annotation.SmartTable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
@SmartTable
@Entity
public class HistroyBeam {
    @SmartColumn(id =0,name = "时间",autoMerge = true)
    @Property(nameInDb = "Time") //时间
    private long time;
    @SmartColumn(id =1,name = "车牌号")
    @Property(nameInDb = "CarNum") //号码
    private String carnum;
    @SmartColumn(id =2,name = "称重值")
    @Property(nameInDb = "Weight") //重量
    private String weight;
    @SmartColumn(id =3,name = "称重次数")
    @Property(nameInDb = "Orr") //称重 次数
    private int cishu;
    @SmartColumn(id =4,name = "人员")
    @Property(nameInDb = "Name") //人员
    public String name;
    @SmartColumn(id =5,name = "皮重")
    @Property(nameInDb = "PiZhong") //皮重
    private String PiZhong;
    @SmartColumn(id =6,name = "毛重")
    @Property(nameInDb = "Maozhong") //毛重
    private String Maozhong;
    @SmartColumn(id =7,name = "净重")
    @Property(nameInDb = "Jingzhong") //净重
    private String Jingzhong;
    @SmartColumn(id =8,name = "运输单位")
    @Property(nameInDb = "yunshu") //毛重
    public String yunshu;
    @SmartColumn(id =9,name = "发货单位")
    @Property(nameInDb = "fahuo") //毛重
    public String fahuo;
    @SmartColumn(id =10,name = "收货单位")
    @Property(nameInDb = "shouhuo") //毛重
    public String shouhuo;
    @Generated(hash = 1130646073)
    public HistroyBeam(long time, String carnum, String weight, int cishu,
            String name, String PiZhong, String Maozhong, String Jingzhong,
            String yunshu, String fahuo, String shouhuo) {
        this.time = time;
        this.carnum = carnum;
        this.weight = weight;
        this.cishu = cishu;
        this.name = name;
        this.PiZhong = PiZhong;
        this.Maozhong = Maozhong;
        this.Jingzhong = Jingzhong;
        this.yunshu = yunshu;
        this.fahuo = fahuo;
        this.shouhuo = shouhuo;
    }
    @Generated(hash = 93650938)
    public HistroyBeam() {
    }
    public long getTime() {
        return this.time;
    }
    public void setTime(long time) {
        this.time = time;
    }
    public String getCarnum() {
        return this.carnum;
    }
    public void setCarnum(String carnum) {
        this.carnum = carnum;
    }
    public String getWeight() {
        return this.weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public int getCishu() {
        return this.cishu;
    }
    public void setCishu(int cishu) {
        this.cishu = cishu;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPiZhong() {
        return this.PiZhong;
    }
    public void setPiZhong(String PiZhong) {
        this.PiZhong = PiZhong;
    }
    public String getMaozhong() {
        return this.Maozhong;
    }
    public void setMaozhong(String Maozhong) {
        this.Maozhong = Maozhong;
    }
    public String getJingzhong() {
        return this.Jingzhong;
    }
    public void setJingzhong(String Jingzhong) {
        this.Jingzhong = Jingzhong;
    }
    public String getYunshu() {
        return this.yunshu;
    }
    public void setYunshu(String yunshu) {
        this.yunshu = yunshu;
    }
    public String getFahuo() {
        return this.fahuo;
    }
    public void setFahuo(String fahuo) {
        this.fahuo = fahuo;
    }
    public String getShouhuo() {
        return this.shouhuo;
    }
    public void setShouhuo(String shouhuo) {
        this.shouhuo = shouhuo;
    }



}
