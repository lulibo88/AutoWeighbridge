package com.licheedev.demo.histroy;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

//车辆信息，手动录入
@Entity
public class CarInfo {
    @Property(nameInDb = "CarNum") //号码
    private String carnum;
    @Property(nameInDb = "Driver") //驾驶员
    private String Driver;
    @Property(nameInDb = "Phone") //驾驶员联系电话
    private String Phone;
    @Property(nameInDb = "Fahuo") //发货单位
    private String Fahuo;
    @Property(nameInDb = "Shouhuo") //收货单位
    private String Shouhuo;
    @Property(nameInDb = "Yunshu") //运输单位
    private String Yunshu;
    @Property(nameInDb = "IC") //IC 卡，使用车牌号或者 IC卡做为 唯一标识
    private String IC;
    @Generated(hash = 594525570)
    public CarInfo(String carnum, String Driver, String Phone, String Fahuo,
            String Shouhuo, String Yunshu, String IC) {
        this.carnum = carnum;
        this.Driver = Driver;
        this.Phone = Phone;
        this.Fahuo = Fahuo;
        this.Shouhuo = Shouhuo;
        this.Yunshu = Yunshu;
        this.IC = IC;
    }
    @Generated(hash = 850322869)
    public CarInfo() {
    }
    public String getCarnum() {
        return this.carnum;
    }
    public void setCarnum(String carnum) {
        this.carnum = carnum;
    }
    public String getDriver() {
        return this.Driver;
    }
    public void setDriver(String Driver) {
        this.Driver = Driver;
    }
    public String getPhone() {
        return this.Phone;
    }
    public void setPhone(String Phone) {
        this.Phone = Phone;
    }
    public String getFahuo() {
        return this.Fahuo;
    }
    public void setFahuo(String Fahuo) {
        this.Fahuo = Fahuo;
    }
    public String getShouhuo() {
        return this.Shouhuo;
    }
    public void setShouhuo(String Shouhuo) {
        this.Shouhuo = Shouhuo;
    }
    public String getYunshu() {
        return this.Yunshu;
    }
    public void setYunshu(String Yunshu) {
        this.Yunshu = Yunshu;
    }
    public String getIC() {
        return this.IC;
    }
    public void setIC(String IC) {
        this.IC = IC;
    }

}
